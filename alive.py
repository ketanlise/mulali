from time import sleep
from requests import get as rget
from os import environ
from logging import error as logerror

BASE_URL = environ.get('BASE_URL_OF_BOT', None)
PORT = 8080
try:
    if len(BASE_URL) == 0:
        raise TypeError
    # BASE_URL = BASE_URL.rstrip("/")
except TypeError:
    BASE_URL = None
PORT = environ.get('PORT', None)
while True:
    try:
        for base in BASE_URL.split():
            basse = base.rstrip("/")
            print(basse)
            rget(basse).status_code
        sleep(5)
    except Exception as e:
        logerror(f"alive.py: {e}")
        sleep(600)
        continue
